import _ from 'lodash'; //library used to convert object to an array etc
import React,{Component} from 'react';
import {ListView} from 'react-native';
import {connect} from 'react-redux';
import{employeesFetch} from '../actions';
import ListItem from './ListItem';

class EmployeeList extends Component{


  // Initially this.props will be empty(initial state) as employees list is being fetched
  // Hence we will create a Data source with helper method createDataSource(), if empty this.props, than empty list created
  // If this.props is filled with data than data is fed into createDataSource()
  componentWillMount(){
    this.props.employeesFetch();

    this.createDataSource(this.props);

  }

  //This is called when props are changed to this component, hence will also have access to both old and new props
  //With new props we are again creating a new data source via helper method
  componentWillReceiveProps(nextProps){
    //nextProps are the next set of props that this component will re rendered with
    //this.props are still the old set of props
    //hence here in this method we have access to both old and new set of props
    this.createDataSource(nextProps);
  }

  //This is the helper method to create a data source for ListView
  createDataSource({employees}){
    const ds = new ListView.DataSource({
      rowHasChanged:(r1,r2) => r1 != r2
    });

    this.dataSource = ds.cloneWithRows(employees);
  }

  renderRow(employee){
    return <ListItem employee={employee}/>;
  }

  render(){
    
    return(
      <ListView
      enableEmptySections
      dataSource = {this.dataSource}
      renderRow = {this.renderRow}
      />
    );
  }
}

const mapStateToProps = state => {
  const employees = _.map(state.employees,(val,uid) => {
    return {...val,uid} //hence we will get something like {name:Daniyal,phone:555-555-5555,shift:'Monday',id:sajahsbjahsbdjah} hence we have just appended the value of id to the object
  });
  return {employees};
};

export default connect(mapStateToProps,{employeesFetch})(EmployeeList);