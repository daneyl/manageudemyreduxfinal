import React, { Component } from 'react';
import {Card,CardSection,Button,Input} from './common';
import {connect} from 'react-redux';
import {employeeUpdate,employeeCreate} from '../actions';
import EmployeeForm from './EmployeeForm';

class EmployeeCreate extends Component{

  onButtonPressed(){
    const {name,phone,shift} = this.props;
    this.props.employeeCreate({name,phone,shift:shift || 'Monday'});//if spinner value is unselected than by default it would be Monday
  }

  //below (...this.props) we are passing all the props of this component down to Employee Form component

  render(){
    return(
      <Card>
        <EmployeeForm {...this.props}/>
        <CardSection>
          <Button onPress={this.onButtonPressed.bind(this)}>
            Create
          </Button>
        </CardSection>
      </Card>
    );
  };
}

const mapStateToProps = state => {
  const {name,phone,shift} = state.employeeForm;
  return {name,phone,shift};
}

export default connect(mapStateToProps,{employeeUpdate,employeeCreate})(EmployeeCreate);