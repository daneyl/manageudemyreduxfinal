import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER
} from './types';
import firebase from 'firebase';
import {Actions} from 'react-native-router-flux';

export const emailChanged = (text) => {
  return{
    type:EMAIL_CHANGED,
    payload:text
};
};

export const passwordChanged = (text) => {
  return{
    type:PASSWORD_CHANGED,
    payload:text
};
};

//for logging in the user asynchronously if the account exist, or create a new account(in catch)
// if account doesnt exists

export const loginUser = ({email,password}) => {
  return(dispatch) => {
    dispatch({
      type:LOGIN_USER
    });

    firebase.auth().signInWithEmailAndPassword(email,password)
    .then(user => loginUserSuccess(dispatch,user)) //login an account
    .catch((error)=>{
      console.log(error);//for reasoning see the reducer AuthReducer.js
      firebase.auth().createUserWithEmailAndPassword(email,password) 
      .then(user => loginUserSuccess(dispatch,user))//create an account
      .catch(() => loginUserFail(dispatch)); //account creation even failed
    });
  };
};

const loginUserFail = (dispatch) => {
  dispatch({
    type:LOGIN_USER_FAIL
  });
}

const loginUserSuccess = (dispatch,user) => {
  dispatch({
    type:LOGIN_USER_SUCCESS,
    payload:user
  });
  Actions.main();
}
  