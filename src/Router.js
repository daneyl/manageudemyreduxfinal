import React from 'react';
import {Scene,Router,Actions} from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit.js';

const RouterComponent = () => {
  return(
    <Router titleStyle={{flex:1,textAlign:'center'}}>
      <Scene key="root" hideNavBar>
        <Scene key="auth">
          <Scene key="login" component={LoginForm} title="Please Login" initial/>
        </Scene>
        <Scene key="main">
          <Scene 
          initial // within main scene employee list should be the first screen
          rightTitle="Add"
          onRight={() => Actions.employeeCreate()}
          key="employeeList" 
          component={EmployeeList} 
          title="Employees"/>
          <Scene key="employeeCreate" title="Create Employee" component={EmployeeCreate}/>
          <Scene key="employeeEdit" title="Edit Employee" component={EmployeeEdit}/>
        </Scene>
      </Scene>
    </Router>
  );
}

export default RouterComponent;