import {
  EMPLOYEE_UPDATE,
  EMPLOYEE_CREATE
} from '../actions/types';

const INITIAL_STATE = {
  name:'',
  phone:'',
  shift:''
};

export default (state = INITIAL_STATE,action) => {
  switch(action.type){

    case EMPLOYEE_UPDATE:
      //This(below) is concept of key-interpretor, where key and value both are not fixed and are dependent
      //upon the values passed to the action creator(in this case)
      return { ...state,[action.payload.prop]:action.payload.value }

    case EMPLOYEE_CREATE:
      return INITIAL_STATE; //setting values to empty once data has been sent to database firebase 

    default:
      return state;
  }
}