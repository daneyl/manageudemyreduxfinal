import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER
} from './../actions/types';

const INITIAL_STATE = {
  email:'',
  password:'',
  user:null,
  error:'',
  loading:false
}; //Setting initial values, if the state is empty!!

export default (state = INITIAL_STATE,action) => {

  console.log(action);
  switch (action.type){

    case EMAIL_CHANGED:
      return {...state,email:action.payload};

    case PASSWORD_CHANGED:
      return {...state,password:action.payload};

    case LOGIN_USER_SUCCESS:
    //adding something here odd like banana; will make run the catch block... in the action creator
      return {...state,user:action.payload,error:'',loading:false};

    case LOGIN_USER_FAIL:
      return {...state, error:'Authentication Failed.',loading:false}

    case LOGIN_USER:
      return {...state,loading:true,error:''}

    default:
      return state; 
  }
};