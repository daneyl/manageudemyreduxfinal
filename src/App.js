import React,{Component} from 'react';
import {Provider} from 'react-redux';
import {createStore,applyMiddleware}from 'redux';
import reducers from './reducers';
import ReduxThunk from 'redux-thunk'; 
import firebase from 'firebase';
import LoginForm from './components/LoginForm';
import Router from './Router';

class App extends Component{

  componentWillMount(){
    // Initialize Firebase
    var config = {
      apiKey: 'AIzaSyD3TF7Mqb7vKb-vE5tlwqaobePRgHhU344',
      authDomain: "manager-de8c2.firebaseapp.com",
      databaseURL: "https://manager-de8c2.firebaseio.com",
      projectId: "manager-de8c2",
      storageBucket: "manager-de8c2.appspot.com",
      messagingSenderId: "344516648226"
    };
    firebase.initializeApp(config);
  }

  render(){

    //the second empty argument is the initial state if we want to pass to our redux application 
    const store = createStore(reducers,{},applyMiddleware(ReduxThunk));

    return(
      <Provider store={store}>
        <Router/>
      </Provider>
    );
  }
}

export default App;